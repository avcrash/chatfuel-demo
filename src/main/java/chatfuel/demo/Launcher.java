package chatfuel.demo;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

/**
 *
 * @author avcrash
 */
public class Launcher {

    private int currentFloor = 1;
    private Boolean doorOpen = false;
    private boolean firstTime = true;

    private Integer maxFloors = 1;
    private Integer speed = 1;
    private Integer height = 1;
    private Integer delay = 1;

    private final static Pattern p = Pattern.compile("^[1-9]+");

    static Logger log = null;

    public static void main(String[] args) {
        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tc] %4$s: %5$s%n");
        log = Logger.getGlobal();
        log.setLevel(Level.INFO);
        new Launcher().waitForInput(args);
    }

    private boolean validateNumber(String value) {
        return p.matcher(value).find();
    }

    private boolean checkFor5AndMore(String value) {
        Matcher m = p.matcher(value);
        if (m.find()) {
            Integer d = Integer.valueOf(value);
            return d >= 5 && d <= 20;
        }
        return false;
    }

    public void waitForInput(String[] args) {
        Options options = new Options();
        String header = "Elevator app";
        try {
            options.addOption("f", "floors", true, "Number of floors, integer [5..20]");
            options.addOption("h", "height", true, "Height of one floor, meters");
            options.addOption("s", "speed", true, "Elevator speed, meters/second");
            options.addOption("d", "delay", true, "Delay between closing/opening doors");

            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);
            HelpFormatter formatter = new HelpFormatter();

            if (cmd.hasOption("f") && checkFor5AndMore(cmd.getOptionValue("f"))
                    && cmd.hasOption("h") && validateNumber(cmd.getOptionValue("h"))
                    && cmd.hasOption("f") && validateNumber(cmd.getOptionValue("f"))
                    && cmd.hasOption("d") && validateNumber(cmd.getOptionValue("d"))) {

                log.log(Level.INFO, "Nice. Launching elevator...");
                launchElevator(cmd.getOptionValue("f"), cmd.getOptionValue("h"), cmd.getOptionValue("s"), cmd.getOptionValue("d"));
            } else {
                formatter.printHelp("elevator", header, options, "", true);
            }

        } catch (Exception e) {

            log.log(Level.SEVERE, "Incorrect usage");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("elevator", header, options, "", true);
        }
    }

    public void launchElevator(String f, String h, String s, String d) {
        try {
            this.maxFloors = Integer.valueOf(f);
            this.delay = Integer.valueOf(d);
            this.height = Integer.valueOf(h);
            this.speed = Integer.valueOf(s);

            if (!doorOpen && firstTime) {
                toggleDoors(doorOpen);
                doorOpen = !doorOpen;
                firstTime = false;
            }

            Scanner scanner = new Scanner(System.in);
            log.log(Level.INFO, "Where, matey?");
            int callFloor = 0;
            boolean loop = true;
            while (!scanner.hasNextInt() || loop) {
                callFloor = scanner.nextInt();
                if (callFloor > 0 && callFloor <= maxFloors) {
                    break;
                } else {
                    log.log(Level.INFO, "Please enter floor between 1 and {0}", maxFloors);
                }
            }

            log.log(Level.INFO, "You entered floor: {0} -> the elevator on {1}", new Object[]{callFloor, currentFloor});
            if (currentFloor == callFloor) {
                if (!doorOpen) {
                    toggleDoors(doorOpen);
                }
                doorOpen = true;
                waitForInsideClick();
            } else {
                moveToTheNeededFloor(callFloor, false);
                waitForInsideClick();
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, e.getCause().toString());
        }
    }

    public void moveToTheNeededFloor(int nextFloor, boolean l) {
        try {
            log.log(Level.INFO, "You chose {0} floor from {1} openDoors {2}", new Object[]{nextFloor, currentFloor, doorOpen});
            if (doorOpen) {
                toggleDoors(doorOpen);
            }

            boolean up = nextFloor - currentFloor > 0;
            long d = new BigDecimal(height).divide(BigDecimal.valueOf(speed), MathContext.DECIMAL64).multiply(BigDecimal.valueOf(1000L)).longValue();

            int diff = up ? nextFloor - currentFloor : currentFloor - nextFloor;
            int floorCount = 0;
            while (diff != 0) {
                diff--;
                floorCount++;
                Thread.sleep(d);
                log.log(Level.INFO, "{0} {1}", new Object[]{up ? "\u2191" : "\u2193", up ? nextFloor - diff : currentFloor - floorCount});
            }
            doorOpen = false;
            toggleDoors(doorOpen);

            currentFloor = nextFloor;
            if (l) {
                launchElevator(maxFloors.toString(), height.toString(), speed.toString(), delay.toString());
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, e.getCause().toString());
        }
    }

    public void toggleDoors(boolean isClose) {
        try {
            log.log(Level.INFO, "{0} doors", isClose ? "Closing " : "Opening ");
            Thread.sleep(delay * 1000);
            log.log(Level.INFO, "Doors are {0}", isClose ? " closed" : "open");
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getCause().toString());
        }
    }

    public void waitForInsideClick() {
        try {
            Scanner scanner = new Scanner(System.in);
            log.log(Level.INFO, "Where, matey?");
            doorOpen = true;

            boolean loop = true;
            int callFloor = 0;
            while (!scanner.hasNextInt() || loop) {
                callFloor = scanner.nextInt();
                if (callFloor > 0 && callFloor <= maxFloors) {
                    break;
                } else {
                    log.log(Level.INFO, "Please enter floor between 1 and {0}", maxFloors);
                }
            }

            moveToTheNeededFloor(callFloor, true);
        } catch (Exception e) {
            log.log(Level.SEVERE, e.getCause().toString());
        }
    }
}
