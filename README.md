# chatfuel-demo

###Usage:  
elevator [-d <arg>] [-f <arg>] [-h <arg>] [-s <arg>]  
  
 -d,--delay <arg>    Delay between closing/opening doors  
 -f,--floors <arg>   Number of floors, integer  
 -h,--height <arg>   Height of one floor, meters  
 -s,--speed <arg>    Elevator speed, meters/second  
 
###Run:  
`mvn clean install`  
and launch jar via:  
`java -jar target/cf-demo.jar -f 20 -h 1 -s 1 -d 1`